#!/usr/bin/env bash

source ./scripts/environmets.sh > /dev/null 2>&1 || source environmets.sh > /dev/null 2>&1

# Global functions
function Done() {
    echo -e '==> \E[32m'"\033\done\033[0m"
}

function Skip() {
    echo -e '==> \E[32m'"\033\skipped\033[0m"
}

function Failed() {
    echo -e '==> \E[91m'"\033\ failed\033[0m"
}

function GetConfirmation() {
    while true
    do
    echo -e '\E[96m'"\033\ Do you want to continue (Yes or No): \033[0m \c"
    read  CONFIRMATION
    case $CONFIRMATION in
    Yes|yes|YES|YeS|yeS|yEs) break ;;
    No|no|NO|nO)
    echo "Exiting..."
    sleep 1
    exit
    ;;
    *) echo "" && echo -e '\E[91m'"\033\Please type Yes or No \033[0m"
    esac
    done
    echo "Continue..."
    sleep 1
}

function EchoDash() {
echo "----------------------------------------------------------------"
}

function FinisMessage() {
        echo ""
        echo -e '\E[1m'"\033\Zabbix installation successfuly finished.\033[0m"
        echo "-----------------------------------------------------------------"
        echo ""
        echo -e '\E[1m'"\033\Zabbix UI is accessible at https://ip:8443 \033[0m"
        echo -e '\E[1m'"\033\Username: Admin \033[0m"
        echo -e '\E[1m'"\033\Pasword: zabbix (Don't forget to change it!)\033[0m"
        echo ""
        echo -e '\E[1m'"\033\Grafana UI is accessible at https://ip:3000 \033[0m"
        echo -e '\E[1m'"\033\Username: admin \033[0m"
        echo -e '\E[1m'"\033\Pasword: zabbix (Don't forget to change it too!)\033[0m"
        echo "-----------------------------------------------------------------"
        echo ""
        echo -e '\E[1m'"\033\For any contribution or issue reporting please visit https://bitbucket.org/secopstech/zabbix-server/issues.\033[0m"
}

function InstallDependenciesCentOS() {
        # Install dependencies for CentOS
        echo -e ""
        echo -e '\E[96m'"\033\- Install dependencies.\033[0m"
        sleep 1
        #check if epel repo installed
        EPEL=$(rpm -qa |egrep epel-release || echo "epel-release is not installed")
        if [[ $EPEL == "epel-release is not installed" ]]; then
            yum install -y epel-release
            echo -n "Enable epel repo:" && \
            echo -ne "\t\t\t\t" && Done
        else
        echo -n "Epel repo is already enabled:" && \
        echo -ne "\t\t\t" && Skip
        fi

        #check if jq installed
        JQ=$(rpm -qa |egrep "^jq" || echo "jq is not installed")
        if [[ $JQ == "jq is not installed" ]]; then
            yum install -y jq
            echo -n "Install jq:" && \
            echo -ne "\t\t\t\t" && Done
            sleep 1
            EchoDash
        else
        echo -n "jq is already installed:" && \
        echo -ne "\t\t\t" && Skip
        sleep 1
        EchoDash
        fi

        #check if openssl installed
        OSSL=$(rpm -qa |egrep "^openssl" || echo "openssl is not installed")
        if [[ $OSSL == "openssl is not installed" ]]; then
            yum install -y openssl
            echo -n "Install openssl:" && \
            echo -ne "\t\t\t\t" && Done
            sleep 1
            EchoDash
        else
        echo -n "openssl is already installed:" && \
        echo -ne "\t\t\t" && Skip
        sleep 1
        EchoDash
        fi

        # Install docker engine if it's not installed
        echo -e ""
        echo -e '\E[96m'"\033\- Install Docker CE. \033[0m"

        check_docker=$(rpm -qa |egrep "docker-ce" || echo "Docker not installed")
        if [[ $check_docker == "Docker not installed" ]]; then
            yum install -y yum-utils device-mapper-persistent-data lvm2
            yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
            yum -y install docker-ce
            systemctl enable docker && systemctl start docker
            echo -n "Docker installation:" && \
            echo -ne "\t\t\t" && Done
            sleep 1
            EchoDash
        else
            echo -n "Docker engine is already installed." && \
            echo -ne "\t\t" && Skip
            sleep 1
            EchoDash
        fi

        # Install docker-compose if it's not installed
        echo -e ""
        echo -e '\E[96m'"\033\- Install Docker Compose. \033[0m"
        if [ ! -x "/usr/local/bin/docker-compose" ]; then
            LATEST_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | jq -r '.tag_name')
            curl -L "https://github.com/docker/compose/releases/download/$LATEST_VERSION/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
            chmod +x /usr/local/bin/docker-compose
            echo -n "Docker Compose installation:" && \
            echo -ne "\t\t\t" && Done
            sleep 1
            EchoDash
        else
            echo -n "Docker compose is already installed." && \
            echo -ne "\t\t" && Skip
            sleep 1
            EchoDash
        fi
    }

function InstallDependenciesUbuntu() {
        # Install dependencies for Ubuntu
        echo -e ""
        echo -e '\E[96m'"\033\- Install dependencies.\033[0m"
        sleep 1
        apt update
        #check if jq installed
        JQ=$(dpkg -l jq > /dev/null 2>&1 || echo "jq is not installed")
        if [[ $JQ == "jq is not installed" ]]; then
            apt install -y jq
            echo -n "Install jq:" && \
            echo -ne "\t\t\t\t" && Done
            sleep 1
            EchoDash
        else
        echo -n "jq is already installed:" && \
        echo -ne "\t\t\t" && Skip
        sleep 1
        EchoDash
        fi

        #check if openssl installed
        OSSL=$(dpkg -l openssl > /dev/null 2>&1 || echo "openssl is not installed")
        if [[ $OSSL == "openssl is not installed" ]]; then
            apt install -y openssl
            echo -n "Install openssl:" && \
            echo -ne "\t\t\t\t" && Done
            sleep 1
            EchoDash
        else
        echo -n "openssl is already installed:" && \
        echo -ne "\t\t\t" && Skip
        sleep 1
        EchoDash
        fi

        # Install docker engine if it's not installed
        echo -e ""
        echo -e '\E[96m'"\033\- Install Docker CE. \033[0m"

        check_docker=$(dpkg -l docker-ce > /dev/null 2>&1 || echo "Docker not installed")
        if [[ $check_docker == "Docker not installed" ]]; then
            apt install -y apt-transport-https ca-certificates curl gnupg lsb-release
            install -m 0755 -d /etc/apt/keyrings
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
            chmod a+r /etc/apt/keyrings/docker.gpg
            echo \
            "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
            "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
            tee /etc/apt/sources.list.d/docker.list > /dev/null
            apt-get update
            apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
            systemctl enable docker && systemctl start docker
            echo -n "Docker installation:" && \
            echo -ne "\t\t\t" && Done
            sleep 1
            EchoDash
        else
            echo -n "Docker engine is already installed." && \
            echo -ne "\t\t" && Skip
            sleep 1
            EchoDash
        fi

        # Install docker-compose if it's not installed
        echo -e ""
        echo -e '\E[96m'"\033\- Install Docker Compose. \033[0m"
        if [ ! -x "/usr/local/bin/docker-compose" ]; then
            LATEST_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | jq -r '.tag_name')
            curl -L "https://github.com/docker/compose/releases/download/$LATEST_VERSION/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
            chmod +x /usr/local/bin/docker-compose
            echo -n "Docker Compose installation:" && \
            echo -ne "\t\t\t" && Done
            sleep 1
            EchoDash
        else
            echo -n "Docker compose is already installed." && \
            echo -ne "\t\t" && Skip
            sleep 1
            EchoDash
        fi
    }

function GetZabbixServerPrimaryIP() {
    default_iface=$(awk '$2 == 00000000 { print $1 }' /proc/net/route)
    ZBX_IP=$(ip addr show dev "$default_iface" | awk '$1 == "inet" { sub("/.*", "", $2); print $2 }')
}

function CheckZabbix() {
    cd $BASEDIR
    status=$(docker-compose ps |egrep zabbix-server |egrep " Up " || echo "Not deployed")
    }

function GetZabbixAuthToken() {
    ZBX_AUTH_TOKEN=$(curl --insecure -s \
    -H "Accept: application/json" \
    -H "Content-Type:application/json" \
     -X POST -d \
     '{"jsonrpc":"2.0",
     "method":"user.login",
     "params":
     {"user":"Admin",
     "password":"zabbix"},
     "auth":null,"id":0}' \
     $ZBX_SERVER_URL/api_jsonrpc.php |jq .result |tr -d '"')
    }

# Create a host group for Windows servers
function CreateHostGroups() {

for i in "${HOST_GROUPS[@]}"
do
PD=$(cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "hostgroup.create",
    "params": {
        "name": "$i"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
)

POST=$(curl -s --insecure \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-X POST --data "$PD" "$ZBX_SERVER_URL/api_jsonrpc.php"  |jq .)

if [[ "$POST" == *"error"* ]]; then
    if [[ "$POST" == *"already exists"* ]]; then
        echo -n "$i already exists." && \
        echo -ne "\t\t\t" && Skip
    else
        echo -n "An error occured. Please check the error output." && \
        echo $POST |jq .
        echo -ne "\t\t" && Failed
    fi
else
echo -n "$i:" && \
echo -ne "\t\t\t\t\t" && Done
sleep 1
fi
done
}

# Create Auto Registration for Linux hosts
function AutoRegisterLinuxPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Linux Servers",
        "eventsource": 2,
        "status": 0,
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": "24",
                    "operator": "2",
                    "value": "Linux"
                }
            ]
        },
            "operations": [
                {
                    "operationtype": "2"
                },
            	{
                    "operationtype": "4",
                    "opgroup": [
                        {
                            "groupid": "2"
                        }
                    ]
                },
                {
                    "operationtype": "6",
                    "optemplate": [
                        {
                            "templateid": "10001"
                        }
                    ]
                }
            ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Get win host group id
function WinHostGroupIDPD() {
cat <<EOF
{ "jsonrpc": "2.0",
          "method": "hostgroup.get",
          "params": {
            "output": "extend",
            "filter": {
                "name": [
                    "Windows servers"
                ]
            }
          },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Get host group ids
function HostGroupIDSPD() {
cat <<EOF
{ "jsonrpc": "2.0",
          "method": "hostgroup.get",
          "params": {
            "output": "extend",
            "filter": {
                "name": [
                    "${HOST_GROUPS[0]}",
                    "${HOST_GROUPS[1]}",
                    "${HOST_GROUPS[2]}",
                    "${HOST_GROUPS[3]}",
                    "${HOST_GROUPS[4]}",
                    "${HOST_GROUPS[5]}",
                    "${HOST_GROUPS[6]}",
                    "${HOST_GROUPS[7]}",
                    "${HOST_GROUPS[8]}",
                    "${HOST_GROUPS[9]}",
                    "${HOST_GROUPS[10]}",
                    "${HOST_GROUPS[11]}",
                    "${HOST_GROUPS[12]}",
                    "${HOST_GROUPS[13]}",
                    "${HOST_GROUPS[14]}",
                    "${HOST_GROUPS[15]}",
                    "${HOST_GROUPS[16]}",
                    "${HOST_GROUPS[17]}",
                    "${HOST_GROUPS[18]}",
                    "${HOST_GROUPS[19]}",
                    "${HOST_GROUPS[20]}",
                    "${HOST_GROUPS[21]}",
                    "${HOST_GROUPS[22]}",
                    "${HOST_GROUPS[23]}",
                    "${HOST_GROUPS[24]}",
                    "${HOST_GROUPS[25]}",
                    "${HOST_GROUPS[26]}"
                ]
            }
          },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Create Auto Registration for Windows hosts
function AutoRegisterWinPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "action.create",
    "params": {
        "name": "Windows Servers",
        "eventsource": 2,
        "status": 0,
        "filter": {
            "evaltype": 0,
            "conditions": [
                {
                    "conditiontype": "24",
                    "operator": "2",
                    "value": "Windows"
                }
            ]
        },
            "operations": [
                {
                    "operationtype": "2"
                },
            	{
                    "operationtype": "4",
                    "opgroup": [
                        {
                            "groupid": "$WGROUPID"
                        }
                    ]
                },
                {
                    "operationtype": "6",
                    "optemplate": [
                        {
                            "templateid": "10081"
                        }
                    ]
                }
            ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

########## ITEM AND TRIGGER CONFIGURATIONS ##########

# Set interval to 5m for Number of Cpus item for linux template (also we're going to remove its current preprocessing
# in order to get this value from Grafana
function NumberofCpusIntervalPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "item.update",
    "params": {
    	"hostid": "10001",
        "itemid": "42220",
        "delay": "5m",
        "preprocessing": [
        ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}


# Set interval to 30m for FS discovery in linux template
function LLDFSRuleLinuxPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "42273",
        "delay": "30m"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Set interval to 30m for Block Device discovery in linux template
function LLDBDRuleLinuxPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "42272",
        "delay": "30m"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Set interval to 30m for network interface discovery in linux template
function LLDNetIfRuleLinuxPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "42271",
        "delay": "30m"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Set interval to 30m for FS discovery for Windows template
function LLDFSRuleWinPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
        "itemid": "42490",
        "delay": "30m"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Disable  annoying service discovery rules which creates too mant false positive alerts.
function DisableAnnoyingWinServiceDiscovery() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "discoveryrule.update",
    "params": {
		"hostid": "10081",
        "itemid": "42489",
        "status": "1"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

##########  NOTIFICATIOS CONFIGURATIONS ##########

# Email related functions
function GetSMTPNotifAnswer() {
    while true
        do
        echo -e '\E[96m'"\033\ Do you want to enable email notification ? (Yes or No): \033[0m \c"
        read  SMTPEnable
        case $SMTPEnable in
        Yes|yes|YES|YeS|yeS|yEs) break ;;
        No|no|NO|nO) break ;;
        *) echo -e '\E[91m'"\033\ Please type Yes or No \033[0m"
        esac
        done
        if [[ "$SMTPEnable" =~ $yesPattern ]]; then

            # SMTP server configuration to send notifications
            echo -e ""
            echo -e '\E[96m'"\033\- Zabbix SMTP settings. \033[0m"
            echo -e '\E[1m'"\033\ SMTP server settings will be configured to send notifications emails.\033[0m"
            echo -e '\E[1m'"\033\ Please provide your SMTP server IP( or host), Port, sender email\033[0m"
            echo -e '\E[1m'"\033\ security prefrence and auth credentials\033[0m"
            sleep 1
            echo ""
            echo -e '\E[96m'"\033\ Enter SMTP Server Address: \033[0m \c"
            read SMTPServer

            echo -e '\E[96m'"\033\ Enter  SMTP Server Port:\033[0m \c"
            read SMTPServerPort
            Integer='^[0-9]+$'
            if ! [[ $SMTPServerPort =~ $Integer ]] ; then
                while ! [[ "$SMTPServerPort" =~ $Integer ]]
                do
                    echo -e '\E[91m'"\033\ Port number should be a number! Please re-enter: \033[0m \c"
                    read SMTPServerPort
                done
            fi

            echo -e '\E[96m'"\033\ Enter SMTP Hello: \033[0m \c"
            read SMTPHello

            echo -e '\E[96m'"\033\ Enter Sender Email: \033[0m \c"
            read SMTPEmail

            while true
                do
                echo -e '\E[96m'"\033\ Enable connection security ? (Yes or No): \033[0m \c"
                read  SecureConnection
                case $SecureConnection in
                Yes|yes|YES|YeS|yeS|yEs) break ;;
                No|no|NO|nO) break ;;
                *) echo -e '\E[91m'"\033\ Please type Yes or No \033[0m"
                esac
                done
            if [ "$SecureConnection" == "No" ] || [  "$SecureConnection" == "no" ] || [  "$SecureConnection" == "NO" ] || [  "$SecureConnection" == "nO" ]; then
                SecureConnection=0
            else
                while true
                do
                echo -e '\E[96m'"\033\ Enter connection security type? (STARTTLS or SSL/TLS): \033[0m \c"
                read SecurityType
                case $SecurityType in
                STARTTLS) break ;;
                SSL/TLS|SSL|TLS) break ;;
                *) echo -e '\E[91m'"\033\Invalid connection security type. Please type STARTTLS or SSL/TLS.\033[0m"
                esac
                done

                if [ "$SecurityType" == "STARTTLS" ]; then
                    SecureConnection=1
                else
                    SecureConnection=2
                fi
            fi

            while true
            do
            echo -e '\E[96m'"\033\ Enable authentication ? (Yes or No): \033[0m \c"
            read  Authentication
            case $Authentication in
            Yes|yes|YES|YeS|yeS|yEs) break ;;
            No|no|NO|nO) break ;;
            *) echo -e '\E[91m'"\033\Please type Yes or No \033[0m"
            esac
            done

            if [ "$Authentication" == "No" ] || [  "$Authentication" == "no" ] || [  "$Authentication" == "NO" ] || [  "$Authentication" == "nO" ]; then
                Authentication=0
            else
                Authentication=1
                echo -e '\E[96m'"\033\ Enter username for SMTP Auth: \033[0m \c"
                read SMTPUsername
                if [[ -z "$SMTPUsername" ]] ; then
                    while [ -z "$SMTPUsername" ]
                    do
                        echo -e '\E[91m'"\033\ Username required! Please enter the username:\033[0m \c"
                        read SMTPUsername
                    done
                fi

                echo -e '\E[96m'"\033\ Enter password for SMTP Auth: \033[0m \c"
                read SMTPPassword
                if [[ -z "$SMTPPassword" ]] ; then
                    while [ -z "$SMTPPassword" ]
                    do
                        echo -e '\E[91m'"\033\ Password required! Please enter the password: \033[0m \c"
                        read SMTPPassword
                    done
                fi
            fi

            # Set admin email to get notifications
            echo -e ""
            echo -e '\E[96m'"\033\- Admin email notification settings. \033[0m"
            echo -e '\E[1m'"\033\ This will set the admin email address to get zabbix alerts,\033[0m"
            echo -e '\E[1m'"\033\ and enable the trigger action for the notifications...\033[0m"
            echo ""
            echo -e '\E[96m'"\033\ Enter an email address for admin user: \033[0m \c"
            read SentTo

            if [[ -z "$SentTo" ]] ; then
                while [ -z "$SentTo" ]
                do
                    echo -e '\E[91m'"\033\ Email address is required!\033[0m"
                    echo -e '\E[91m'"\033\ Please enter an email:\033[0m \c"
                    read SentTo
                done
            fi
        else
            echo -n "Email notification configuration:" && \
            echo -ne "\t\t" && Skip
            sleep 1
        fi
}

function SMTPConfigPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "1",
        "status": 0,
        "smtp_server": "$SMTPServer",
        "smtp_port": "$SMTPServerPort",
        "smtp_helo": "$SMTPHello",
        "smtp_email": "$SMTPEmail",
        "smtp_security": $SecureConnection,
        "smtp_authentication": $Authentication,
        "username": "$SMTPUsername",
        "passwd": "$SMTPPassword"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Slack related functions
function GetSlackNotifAnswer(){
    while true
    do
        echo -e '\E[96m'"\033\ Do you want to enable slack notifications ? (Yes or No): \033[0m \c"
        read  SlackEnable
        case $SlackEnable in
        Yes|yes|YES|YeS|yeS|yEs) break ;;
        No|no|NO|nO) break ;;
        *) echo -e '\E[91m'"\033\ Please type Yes or No \033[0m"
        esac
    done
    if [[ "$SlackEnable" =~ $yesPattern ]]; then
        echo -e ""
        echo -e '\E[96m'"\033\- Slack settings. \033[0m"
        echo -e '\E[1m'"\033\This section, enables slack notification. \033[0m"

        echo -e '\E[1m'"\033\An slack app must be created within your Slack.com workspace \033[0m"
        echo -e '\E[1m'"\033\as explained at https://git.zabbix.com/projects/ZBX/repos/zabbix/browse/templates/media/slack \033[0m"
        echo -e '\E[1m'"\033\Please create the slack app now and provide its Bot User OAuth Access Token, and slack channel name.\033[0m"
        echo ""
        sleep 1

        # Get Bot Token
        echo -e '\E[96m'"\033\ Enter your Bot User OAuth Access Token: \033[0m \c"
        read SlackBotToken
        while [[ -z $SlackBotToken ]]
        do
          echo -e '\E[91m'"\033\ Please enter your Bot User OAuth Access Token:\033[0m \c"
          read SlackBotToken
        done

        # Get slack channel name to send notifications
        echo -e '\E[96m'"\033\ Enter your slack channel: \033[0m \c"
        read SlackChannel
        while [[ -z $SlackChannel ]]
        do
          echo -e '\E[91m'"\033\ Please enter your channel:\033[0m \c"
          read SlackChannel
        done
        SlackChannel="#$SlackChannel"
    else
        echo -n "Slack notification configuration:" && \
        echo -ne "\t\t" && Skip
        sleep 1
    fi
}

# Globl Macro for ZABBIX.URL
function ZabbixUrlGlobalMacroPD(){
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "usermacro.createglobal",
    "params":  {
        "macro": "{\$ZABBIX.URL}",
        "value": "https://$ZBX_PUBLIC_IP:8443"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 1
}
EOF
}

# Slack bot token
function SetSlackBotTokenPD(){
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "mediatype.update",
    "params": {
        "mediatypeid": "9",
        "status": 0,
        "parameters": [
                {
                    "name": "zabbix_url",
                    "value": "{\$ZABBIX.URL}"
                },
                {
                    "name": "bot_token",
                    "value": "$SlackBotToken"
                },
                {
                    "name": "channel",
                    "value": "{ALERT.SENDTO}"
                },
                {
                    "name": "slack_mode",
                    "value": "alarm"
                },
                {
                    "name": "slack_as_user",
                    "value": "true"
                },
                {
                    "name": "event_tags",
                    "value": "{EVENT.TAGS}"
                },
                {
                    "name": "event_nseverity",
                    "value": "{EVENT.NSEVERITY}"
                },
                {
                    "name": "event_value",
                    "value": "{EVENT.VALUE}"
                },
                {
                    "name": "event_update_status",
                    "value": "{EVENT.UPDATE.STATUS}"
                },
                {
                    "name": "event_date",
                    "value": "{EVENT.DATE}"
                },
                {
                    "name": "event_time",
                    "value": "{EVENT.TIME}"
                },
                {
                    "name": "event_severity",
                    "value": "{EVENT.SEVERITY}"
                },
                {
                    "name": "event_opdata",
                    "value": "{EVENT.OPDATA}"
                },
                {
                    "name": "event_id",
                    "value": "{EVENT.ID}"
                },
                {
                    "name": "trigger_id",
                    "value": "{TRIGGER.ID}"
                },
                {
                    "name": "trigger_description",
                    "value": "{TRIGGER.DESCRIPTION}"
                },
                {
                    "name": "host_name",
                    "value": "{HOST.HOST}"
                },
                {
                    "name": "event_update_date",
                    "value": "{EVENT.UPDATE.DATE}"
                },
                {
                    "name": "event_update_time",
                    "value": "{EVENT.UPDATE.TIME}"
                },
                {
                    "name": "event_recovery_date",
                    "value": "{EVENT.RECOVERY.DATE}"
                },
                {
                    "name": "event_recovery_time",
                    "value": "{EVENT.RECOVERY.TIME}"
                },
                {
                    "name": "alert_message",
                    "value": "{ALERT.MESSAGE}"
                },
                {
                    "name": "alert_subject",
                    "value": "{ALERT.SUBJECT}"
                },
                {
                    "name": "discovery_host_dns",
                    "value": "{DISCOVERY.DEVICE.DNS}"
                },
                {
                    "name": "discovery_host_ip",
                    "value": "{DISCOVERY.DEVICE.IPADDRESS}"
                },
                {
                    "name": "event_source",
                    "value": "{EVENT.SOURCE}"
                },
                {
                    "name": "host_conn",
                    "value": "{HOST.CONN}"
                }
            ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 1
}
EOF
}

# User media type configuration
function AdminSmtpMediaTypePD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "user_medias": [
            {
                "mediatypeid": "1",
                "sendto": "$SentTo",
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

function AdminSlackMediaTypePD(){
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "user_medias": [
            {
                "mediatypeid": "9",
                "sendto": "$SlackChannel",
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

function AdminSmtpSlackMediaTypePD(){
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.update",
    "params": {
        "userid": "1",
        "user_medias": [
            {
                "mediatypeid": "9",
                "sendto": "$SlackChannel",
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            },
            {
                "mediatypeid": "1",
                "sendto": "$SentTo",
                "active": 0,
                "severity": 63,
                "period": "1-7,00:00-24:00"
            }
        ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Notification trigger action for administrators
function NotifTriggerPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "action.update",
    "params": {
        "actionid": 3,
        "status": 0
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# API related
function GetAPIUserGroupIDPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "usergroup.get",
    "params": {
        "output": "extend",
        "status": 0
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 1
}
EOF
}

# Add user group for zabbix api user
function CreateAPIUserGroupPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "usergroup.create",
    "params": {
        "name": "API Users",
        "gui_access": 3,
		"users_status": 0,
		"rights": [
			{
				"permission": 2,
		    	"id": "2"
		    },
		    {
				"permission": 2,
		    	"id": "4"
		    },
			{
				"permission": 2,
		    	"id": "5"
		    },
			{
				"permission": 2,
		    	"id": "6"
		    },
			{
				"permission": 2,
		    	"id": "7"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[0]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[1]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[2]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[3]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[4]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[5]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[6]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[7]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[8]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[9]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[10]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[11]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[12]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[13]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[14]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[15]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[16]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[17]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[18]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[19]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[20]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[21]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[22]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[23]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[24]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[25]}"
		    },
		    {
		    	"permission": 2,
		    	"id": "${GRP_IDS_ARRAY[26]}"
		    }
		]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

function CreateAPIUserPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.create",
    "params": {
        "alias": "apiuser",
        "passwd": "IdFthQ1S.ZRtr",
        "roleid": "1",
        "usrgrps": [
            {
                "usrgrpid": "$API_USERS_GROUP_ID"
            }
        ]
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

# Zabbix server Host ID
function GetHostIDPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": "extend"
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

function UpdateHostInterfacePD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "hostinterface.update",
    "params": {
        "interfaceid": "1",
        "hostids": "$ZBX_AGENT_HOST_ID",
        "type": 1,
        "useip": 0,
        "dns": "zabbix-agent",
        "port": 10050,
        "main": 1
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

function EnableZbxAgentonServerPD() {
cat <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.update",
    "params": {
        "hostid": "$ZBX_AGENT_HOST_ID",
        "host": "$ZBX_AGENT_CONTAINER_ID",
        "name": "Zabbix server",
        "status": 0
    },
    "auth": "$ZBX_AUTH_TOKEN",
    "id": 0
}
EOF
}

### Grafana related
function CreateGRFAPIKey() {
    GRF_API_KEY=$(curl --insecure -s \
    -H "Accept: application/json" \
    -H "Content-Type:application/json" \
    -X POST -d \
     '{
	    "name":"zabbix-api-key",
	    "role": "Admin"
      }' \
     $GRF_SERVER_URL/api/auth/keys |jq .key |tr -d '"')
}

function CreateZbxDataSourcePD() {
cat <<EOF
{
        "orgId": 1,
        "name": "zabbix",
        "type": "alexanderzobnin-zabbix-datasource",
        "typeLogoUrl": "public/plugins/alexanderzobnin-zabbix-datasource/img/zabbix_app_logo.svg",
        "access": "proxy",
        "url": "https://zabbix-web-nginx-mysql:8443/api_jsonrpc.php",
        "password": "IdFthQ1S.ZRtr",
        "user": "apiuser",
        "database": "",
        "basicAuth": false,
        "isDefault": true,
        "jsonData": {
            "dbConnection": {
                "enable": false
            },
            "keepCookies": [],
            "password": "IdFthQ1S.ZRtr",
            "tlsSkipVerify": true,
            "username": "apiuser",
            "cacheTTL": "5m"
        },
        "readOnly": false
}
EOF
}

